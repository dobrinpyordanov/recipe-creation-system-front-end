# Recipe Creation System Front-End

## Description

This is Team 13's front-end for the final project which Fourth gave to part of Telerik Academy Alpha's JavaScript cohort. Our aim is to design and implement a single-page web app which would allow restaurant chefs to create and manage recipes. Recipes will be composed of products with a known nutritional value. During recipe composition chefs should see changes in total nutritional value in real time - each time an ingredient is added, removed, or its quantity modified. Newly created recipes could be tagged with a category from a pre-defined list.

### Tasks

- Authentication - Log in
- Read a list of previously created recipes
- CRUD individual recipes which contain products and/or other recipes

---

## Getting started

### Installation

Clone the repository

    git clone https://gitlab.com/dobrinpyordanov/recipe-creation-system-front-end.git

Enter its folder

    cd recipe-creation-system-front-end/

Install project dependencies

    npm install

Start the project

    ng s

Log in with the following credentials

    Username: TestUser
    Password: Test1234%

You can create a new recipe by clicking the `Create Recipe` button in the nav-bar

On the create-recipe screen you can name the recipe. You can also add ingredients or recipes to the recipe by selecting one of the buttons under the recipe name. When adding a product, you can either specify its food group and then search products only from that food group, or you can directly search all available products. After you've selected a product, you can choose which of its available measurement types to use, and then you can set a quantity for it of that type. You can click the `more` link under each product to see a full list of its nutritional values. You can also expand the `current nutritions` section which will show all nutritional values for the recipe overall. You can then add a freeform category of the recipe and hit the `Create recipe` button. If you wish to back out of recipe creation at any time, and discard your changes, you can click the `Recipes` button in the nav-bar - this will return you to the list of recipes.

You can also click one of the existing recipes to see its details. From this screen you can edit or delete the recipe. Editing a recipe brings you to a version of the create-recipe screen which allows you to modify its contents or add new ingredients/recipes to it. Once you're done, you can save your changes with the `Update recipe` button. You can also back out and discard your changes with the `Cancel` button which brings you back to the view-recipe-details screen. Selecting the `Delete` button brings up a prompt which, if confirmed, deletes the recipe.

When you're finished working with the system, you can log out by clicking the corresponding button in the top right part of the screen, which brings you back to the log in page.
