import { IMeasure } from 'src/app/common/interfaces/measurement.interface';
import { INutrition } from 'src/app/common/interfaces/nutrition.interface';

export class ProductDTO {
  public description: string;
  public foodGroup: string;
  public measures: IMeasure[];
  public nutrition: INutrition;
}
