import { ICreateRecipeProduct } from 'src/app/common/interfaces/createRecipeProduct.interface';
import { ISelectedNutrient } from 'src/app/common/interfaces/selectedNutrient.interface';

export class CreateRecipeDTO {
  id?: string;

  title?: string;

  recipeProducts?: ICreateRecipeProduct[];

  childRecipes?: any[];

  nutritionalValue: ISelectedNutrient[];

  recipeCategory?: string;
}
