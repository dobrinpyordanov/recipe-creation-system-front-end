import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import { catchError } from 'rxjs/operators';
import { of, Observable } from 'rxjs';
import { NotificatorService } from '../../core/services/notificator.service';
import { RecipeService } from '../recipe.service';

@Injectable({
  providedIn: 'root'
})
export class RecipesResolverService implements Resolve<any> {
  constructor(
    private readonly recipeService: RecipeService,
    public readonly notificator: NotificatorService
  ) {}

  public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.recipeService.getAllRecipes().pipe(
      catchError(res => {
        this.notificator.error('error with getting recipes');
        return of({ posts: [] });
      })
    );
  }
}
