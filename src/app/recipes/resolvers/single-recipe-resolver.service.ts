import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import { catchError } from 'rxjs/operators';
import { of, Observable } from 'rxjs';
import { NotificatorService } from '../../core/services/notificator.service';
import { RecipeService } from '../recipe.service';

@Injectable({
  providedIn: 'root'
})
export class SingleRecipeResolverService implements Resolve<any> {
  constructor(
    private readonly recipeService: RecipeService,
    public readonly notificator: NotificatorService
  ) {}

  public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.recipeService.getRecipeById(route.params.id).pipe(
      catchError(res => {
        this.notificator.error('error with getting recipes');
        return of({ posts: [] });
      })
    );
  }

  // public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
  //   return of([
  //     {
  //       product: {
  //         description: 'ktarator',
  //         foodGroup: 'string',
  //         measures: [{ measure: 'spoon', gramsPerMeasure: 9 }],
  //         nutrition: {
  //           PROCNT: {
  //             desciption: 'string',
  //             unit: 'string',
  //             value: 6
  //           },
  //           FAT: {
  //             desciption: 'string',
  //             unit: 'string',
  //             value: 6
  //           },
  //           CHOCDF: {
  //             desciption: 'string',
  //             unit: 'string',
  //             value: 6
  //           },
  //           ENERC_KCAL: {
  //             desciption: 'string',
  //             unit: 'string',
  //             value: 6
  //           },
  //           SUGAR: {
  //             desciption: 'string',
  //             unit: 'string',
  //             value: 6
  //           },
  //           FIBTG: {
  //             desciption: 'string',
  //             unit: 'string',
  //             value: 6
  //           },
  //           CA: {
  //             desciption: 'string',
  //             unit: 'string',
  //             value: 6
  //           },
  //           FE: {
  //             desciption: 'string',
  //             unit: 'string',
  //             value: 6
  //           },
  //           P: {
  //             desciption: 'string',
  //             unit: 'string',
  //             value: 6
  //           },
  //           K: {
  //             desciption: 'string',
  //             unit: 'string',
  //             value: 6
  //           },
  //           NA: {
  //             desciption: 'string',
  //             unit: 'string',
  //             value: 6
  //           },
  //           VITA_IU: {
  //             desciption: 'string',
  //             unit: 'string',
  //             value: 6
  //           },
  //           TOCPHA: {
  //             desciption: 'string',
  //             unit: 'string',
  //             value: 6
  //           },
  //           VITD: {
  //             desciption: 'string',
  //             unit: 'string',
  //             value: 6
  //           },
  //           VITC: {
  //             desciption: 'string',
  //             unit: 'string',
  //             value: 6
  //           },
  //           VITB12: {
  //             desciption: 'string',
  //             unit: 'string',
  //             value: 6
  //           },
  //           FOLAC: {
  //             desciption: 'string',
  //             unit: 'string',
  //             value: 6
  //           },
  //           CHOLE: {
  //             desciption: 'string',
  //             unit: 'string',
  //             value: 6
  //           },
  //           FATRN: {
  //             desciption: 'string',
  //             unit: 'string',
  //             value: 6
  //           },
  //           FASAT: {
  //             desciption: 'string',
  //             unit: 'string',
  //             value: 6
  //           },
  //           FAMS: {
  //             desciption: 'string',
  //             unit: 'string',
  //             value: 6
  //           },
  //           FAPU: {
  //             desciption: 'string',
  //             unit: 'string',
  //             value: 6
  //           }
  //         }
  //       },
  //       measurment: 'cup',
  //       amount: 9
  //     },
  //     {
  //       product: {
  //         description: 'bobec',
  //         foodGroup: 'spices',
  //         measures: [
  //           { measure: 'spoon', gramsPerMeasure: 9 },
  //           { measure: 'cup', gramsPerMeasure: 25 }
  //         ],
  //         nutrition: {
  //           PROCNT: {
  //             desciption: 'string',
  //             unit: 'string',
  //             value: 6
  //           },
  //           FAT: {
  //             desciption: 'string',
  //             unit: 'string',
  //             value: 6
  //           },
  //           CHOCDF: {
  //             desciption: 'string',
  //             unit: 'string',
  //             value: 6
  //           },
  //           ENERC_KCAL: {
  //             desciption: 'string',
  //             unit: 'string',
  //             value: 6
  //           },
  //           SUGAR: {
  //             desciption: 'string',
  //             unit: 'string',
  //             value: 6
  //           },
  //           FIBTG: {
  //             desciption: 'string',
  //             unit: 'string',
  //             value: 6
  //           },
  //           CA: {
  //             desciption: 'string',
  //             unit: 'string',
  //             value: 6
  //           },
  //           FE: {
  //             desciption: 'string',
  //             unit: 'string',
  //             value: 6
  //           },
  //           P: {
  //             desciption: 'string',
  //             unit: 'string',
  //             value: 6
  //           },
  //           K: {
  //             desciption: 'string',
  //             unit: 'string',
  //             value: 6
  //           },
  //           NA: {
  //             desciption: 'string',
  //             unit: 'string',
  //             value: 6
  //           },
  //           VITA_IU: {
  //             desciption: 'string',
  //             unit: 'string',
  //             value: 6
  //           },
  //           TOCPHA: {
  //             desciption: 'string',
  //             unit: 'string',
  //             value: 6
  //           },
  //           VITD: {
  //             desciption: 'string',
  //             unit: 'string',
  //             value: 6
  //           },
  //           VITC: {
  //             desciption: 'string',
  //             unit: 'string',
  //             value: 6
  //           },
  //           VITB12: {
  //             desciption: 'string',
  //             unit: 'string',
  //             value: 6
  //           },
  //           FOLAC: {
  //             desciption: 'string',
  //             unit: 'string',
  //             value: 6
  //           },
  //           CHOLE: {
  //             desciption: 'string',
  //             unit: 'string',
  //             value: 6
  //           },
  //           FATRN: {
  //             desciption: 'string',
  //             unit: 'string',
  //             value: 6
  //           },
  //           FASAT: {
  //             desciption: 'string',
  //             unit: 'string',
  //             value: 6
  //           },
  //           FAMS: {
  //             desciption: 'string',
  //             unit: 'string',
  //             value: 6
  //           },
  //           FAPU: {
  //             desciption: 'string',
  //             unit: 'string',
  //             value: 6
  //           }
  //         }
  //       },
  //       measurment: 'spoon',
  //       amount: 9
  //     }
  //   ]);
  // }
}
