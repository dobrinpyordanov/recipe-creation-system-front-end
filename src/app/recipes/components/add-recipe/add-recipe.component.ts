import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef
} from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable, fromEvent, of } from 'rxjs';
import {
  startWith,
  map,
  filter,
  debounceTime,
  distinctUntilChanged
} from 'rxjs/operators';
import { RecipeService } from '../../recipe.service';
import { CreateRecipeDTO } from '../../interfaces/createRecipeDTO';

@Component({
  selector: 'app-add-recipe',
  templateUrl: './add-recipe.component.html',
  styleUrls: ['./add-recipe.component.css']
})
export class AddRecipeComponent implements OnInit {
  private value = '';
  private recipeControl = new FormControl();
  private apiResponse: any;
  private selectedRecipe: CreateRecipeDTO;
  private detailedNutrients = [];
  // private kcal: number;
  @Output() calories = new EventEmitter<number>();
  @Output() allNutrsFromRecipe = new EventEmitter<any>();
  @Output() recipe = new EventEmitter<CreateRecipeDTO>();
  @ViewChild('recipeSearchInput', { static: false })
  recipeSearchInput: ElementRef;

  constructor(private readonly recipeService: RecipeService) {
    this.apiResponse = [];
  }

  ngOnInit() {
    setTimeout(() => {
      fromEvent(this.recipeSearchInput.nativeElement, 'keyup')
        .pipe(
          map((event: any) => {
            return event.target.value;
          }),
          filter(res => res.length !== 2 && res.length !== 1),
          debounceTime(350),
          distinctUntilChanged()
        )
        .subscribe((text: string) => {
          this.searchGetCall(text).subscribe(
            res => {
              console.log('res', res);
              this.apiResponse = res;
            },
            err => {
              console.log('error', err);
            }
          );
        });
    }, 1000);
  }
  // sendCalories() {
  //   this.calories.emit(Number(this.selectedRecipe.nutritionalValue[3].value));
  // }
  sendNutrients() {
    this.allNutrsFromRecipe.emit(this.detailedNutrients);
  }
  sendRecipe() {
    this.recipe.emit(this.selectedRecipe);
  }
  getRecipe(recipe) {
    const filteredRecipes = this.apiResponse.filter(
      element => element.title === recipe
    );
    this.selectedRecipe = filteredRecipes[0];
    console.log(this.selectedRecipe);
    this.getNutrients();
    // this.sendCalories();
  }
  getNutrients() {
    this.detailedNutrients = this.selectedRecipe.nutritionalValue;
    console.log(this.detailedNutrients);
  }
  searchGetCall(term: string) {
    if (term === '') {
      return of([]);
    }
    return this.recipeService.getRecipeByTitle(term);
  }
}
