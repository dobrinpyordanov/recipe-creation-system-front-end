import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef,
  Input
} from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable, BehaviorSubject, of, fromEvent } from 'rxjs';
import {
  map,
  startWith,
  filter,
  debounceTime,
  distinctUntilChanged
} from 'rxjs/operators';
import { FoodGroups } from 'src/app/common/foodGroup';
import { RecipeService } from 'src/app/recipes/recipe.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { IProduct } from 'src/app/common/interfaces/product.interface';
import { IMeasure } from 'src/app/common/interfaces/measurement.interface';
import { ICreateRecipeProduct } from 'src/app/common/interfaces/createRecipeProduct.interface';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {
  apiResponse: any;
  isFoodGroupSelected = false;
  value = '';
  foodGroupControl = new FormControl();
  myControl = new FormControl();
  myControl2 = new FormControl();
  myControl3 = new FormControl();
  ProductControl = new FormControl();
  showNutrients = false;
  showAllNutrients = false;
  measurment: IMeasure[];
  options: string[] = FoodGroups.foodGroups;
  filteredOptions: Observable<string[]>;
  filteredOptions2: Observable<string[]>;
  ingreds: IProduct[];
  ingredients = [];
  measurements = [];
  detailedNutrients = [];
  selectedIngredient: IProduct;
  selectedMeasure: string;
  selectedAmount = 1;
  gramsPerMeasure: number;
  kcalNow: number;
  fats: number;
  sugars: number;
  carbs: number;
  bNutrients = new BehaviorSubject(this.detailedNutrients);
  bCalories = new BehaviorSubject(this.kcalNow);
  bFats = new BehaviorSubject(this.fats);
  bCarbs = new BehaviorSubject(this.carbs);
  bSugars = new BehaviorSubject(this.sugars);
  @Output() allNutrs = new EventEmitter<any>();
  @Output() calories = new EventEmitter<number>();
  @Output() fat = new EventEmitter<number>();
  @Output() carb = new EventEmitter<number>();
  @Output() sugar = new EventEmitter<number>();
  @ViewChild('productSearchInput', { static: false })
  productSearchInput: ElementRef;
  productReady: ICreateRecipeProduct;
  @Output() productEvent = new EventEmitter<ICreateRecipeProduct>();
  @Output() showDetailed = new EventEmitter<boolean>();
  @Input() productToUpdate: ICreateRecipeProduct;
  measureInputValue;
  productInputValue;
  amountInputValue: number;
  apiSearch = false;
  productIsUntouched = true;
  changeMeasure = false;

  constructor(
    private readonly recipeService: RecipeService // private readonly notificator: NotificatorService
  ) {
    this.apiResponse = [];
  }

  ngOnInit() {
    setTimeout(() => {
      fromEvent(this.productSearchInput.nativeElement, 'keyup')
        .pipe(
          map((event: any) => {
            return event.target.value;
          }),
          filter(res => res.length !== 2 && res.length !== 1),
          debounceTime(350),
          distinctUntilChanged()
        )
        .subscribe((text: string) => {
          this.searchGetCall(text).subscribe(
            res => {
              // console.log('res', res);
              this.apiResponse = res;
            },
            err => {
              // console.log('error', err);
            }
          );
        });
    }, 1000);
    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
    if (this.productToUpdate) {
      // console.log(this.productToUpdate, 'prod to update');
      this.selectedMeasure = this.productToUpdate.measurment;
      this.selectedAmount = this.productToUpdate.amount;
      this.selectedIngredient = this.productToUpdate.product;
      this.gramsPerMeasure = this.productToUpdate.gramsPerSelectedMeasure;
      // console.log(this.selectedMeasure, 'sel measure');
      // console.log(this.selectedIngredient, 'sel ingred');
      // console.log(this.selectedAmount, 'sel amount');
      this.kcalNow =
        this.selectedIngredient.nutrition.ENERC_KCAL.value *
        this.gramsPerMeasure *
        0.01;
      this.bCalories.next(this.kcalNow * this.selectedAmount);
      this.fats =
        this.selectedIngredient.nutrition.FAT.value *
        this.gramsPerMeasure *
        0.01;
      this.bFats.next(this.fats * this.selectedAmount);
      this.sugars =
        this.selectedIngredient.nutrition.SUGAR.value *
        this.gramsPerMeasure *
        0.01;
      this.bSugars.next(this.sugars * this.selectedAmount);
      this.carbs =
        this.selectedIngredient.nutrition.CHOCDF.value *
        this.gramsPerMeasure *
        0.01;
      this.bCarbs.next(this.carbs * this.selectedAmount);
    }
  }
  api() {
    // console.log(this.apiResponse.products);
  }
  searchGetCall(term: string) {
    if (term === '') {
      return of([]);
    }
    return this.recipeService.searchProduct(term);
  }
  sendNutrients() {
    this.allNutrs.emit(this.bNutrients.value);
  }
  sendCalories() {
    this.calories.emit(this.bCalories.value);
  }
  sendFats() {
    this.fat.emit(this.bFats.value);
  }
  sendCarbs() {
    this.carb.emit(this.bCarbs.value);
  }
  sendSugars() {
    this.sugar.emit(this.bSugars.value);
  }
  sendShow() {
    this.showDetailed.emit(true);
  }
  sendProduct() {
    this.productReady = {
      product: this.selectedIngredient,
      measurment: this.selectedMeasure,
      amount: this.selectedAmount,
      gramsPerSelectedMeasure: this.gramsPerMeasure
    };

    // console.log(this.productReady);
    this.productEvent.emit(this.productReady);
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(option =>
      option.toLowerCase().includes(filterValue)
    );
  }

  private _filter3(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.ingredients.filter(option =>
      option.toLowerCase().includes(filterValue)
    );
  }
  getIngredient(ingredient) {
    if (this.ingredients.length > 1) {
      const test1 = this.ingredients.indexOf(ingredient);
      this.selectedIngredient = this.ingreds[test1];
      // console.log(this.selectedIngredient);

      this.measurements = [];
      this.measurment = this.ingreds[test1].measures;
      // tslint:disable-next-line: prefer-for-of
      for (let i = 0; i < this.ingreds[test1].measures.length; i++) {
        this.measurements.push(this.ingreds[test1].measures[i].measure);
      }
    } else {
      const index = this.apiResponse.products.filter(
        element => element.description === ingredient
      );
      this.selectedIngredient = index[0];
      // console.log(this.selectedIngredient);
      this.measurements = [];
      this.measurment = this.selectedIngredient.measures;
      this.measurment.forEach(element => {
        this.measurements.push(element.measure);
      });
    }
  }

  getAllProductsByFoodGroup(foodGroup: string) {
    this.isFoodGroupSelected = true;
    this.recipeService.productsFromFoodGroup(foodGroup).subscribe(data => {
      this.ingreds = data.products;
      this.ingredients = [];
      // tslint:disable-next-line: prefer-for-of
      for (let i = 0; i < this.ingreds.length; i++) {
        this.ingredients.push(this.ingreds[i].description);
      }
      this.filteredOptions2 = this.myControl2.valueChanges.pipe(
        startWith(''),
        map(value => this._filter3(value))
      );
    });
  }
  testM() {
    this.ingredients = [];
  }
  getMeasure(input) {
    const indexOfMeasure = this.measurements.indexOf(input);
    // console.log(input);
    const gramsPerMeasure = this.selectedIngredient.measures[indexOfMeasure]
      .gramsPerMeasure;
    this.gramsPerMeasure = gramsPerMeasure;
    // console.log(gramsPerMeasure, 'grams per selected measure');
    this.selectedMeasure = input;

    const kcalPer100: number = this.selectedIngredient.nutrition.ENERC_KCAL
      .value;
    const kcal = kcalPer100 * 0.01 * gramsPerMeasure;
    this.kcalNow = Math.round(kcal);

    const fatsPer100: number = this.selectedIngredient.nutrition.FAT.value;
    const fat = fatsPer100 * 0.01 * gramsPerMeasure;
    this.fats = Math.round(fat);

    const sugarPer100: number = this.selectedIngredient.nutrition.SUGAR.value;
    const sugar = sugarPer100 * 0.01 * gramsPerMeasure;
    this.sugars = Math.round(sugar);

    const carbsPer100: number = this.selectedIngredient.nutrition.CHOCDF.value;
    const carb = carbsPer100 * 0.01 * gramsPerMeasure;
    this.carbs = Math.round(carb);

    this.showNutrients = true;
    this.bCalories.next(this.kcalNow);
    this.sendCalories();
    this.bFats.next(this.fats);
    this.sendFats();
    this.bCarbs.next(this.carbs);
    this.sendCarbs();
    this.bSugars.next(this.sugars);
    this.sendSugars();
    this.allNutrients();
    this.sendProduct();
  }
  allNutrients() {
    this.mapNutrients();
    // console.log(this.detailedNutrients, 'dateiled');

    this.bNutrients.next(this.detailedNutrients);
    this.sendNutrients();
  }
  close() {
    this.showAllNutrients = false;
  }
  log() {
    // console.log('test');
    // console.log(this.productToUpdate);
    // console.log(this.selectedIngredient);
    // console.log(this.selectedIngredient.description);
    // console.log(this.selectedMeasure);
    // console.log(this.selectedAmount);
  }
  updateNutrients(amount: number) {
    // console.log(amount);

    this.selectedAmount = amount;

    if (this.selectedIngredient) {
      this.bCalories.next(this.kcalNow * amount);
      this.sendCalories();
      this.bFats.next(this.fats * amount);
      this.sendFats();
      this.bCarbs.next(this.carbs * amount);
      this.sendCarbs();
      this.bSugars.next(this.sugars * amount);
      this.sendSugars();
      this.allNutrients();
      this.sendProduct();
    }
  }
  mapNutrients() {
    this.showAllNutrients = !this.showAllNutrients;
    const arr = this.selectedIngredient.nutrition;
    const result = Object.keys(arr).map(key => {
      return [String(key), arr[key]];
    });
    // console.log(result, 'RESULT');

    result.forEach(element => {
      element.shift();
    });
    // console.log(result, 'result after shift');

    const arrayOfNutritions = [];
    result.forEach(element => {
      const nameOfNutrient = element[0].description;
      arrayOfNutritions.push({
        description: element[0].description,
        value: (
          element[0].value *
          0.01 *
          this.selectedAmount *
          this.gramsPerMeasure
        ).toFixed(2),
        unit: element[0].unit
      });
    });
    // const arrayOfValues = [];
    // result.forEach(element => {
    //   arrayOfValues.push(element[0].value);
    // });
    // console.log(arrayOfValues, 'arr  of values');
    // console.log(arrayOfNutritions, 'curently used array of nutrs');
    this.detailedNutrients = arrayOfNutritions;
  }
  toggleUnchanged() {
    this.productIsUntouched = false;
    // console.log(88);
  }
}
