import { IRecipeProduct } from './../../../common/interfaces/recipe-product.interface';
import { INutrient } from './../../../common/interfaces/nutrient.interface';
import { IViewIndividualRecipe } from './../../../common/interfaces/view-individual-recipe.interface';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RecipeService } from '../../recipe.service';

@Component({
  selector: 'app-recipe-details',
  templateUrl: './recipe-details.component.html',
  styleUrls: ['./recipe-details.component.css']
})
export class RecipeDetailsComponent implements OnInit {
  private individualRecipe: IViewIndividualRecipe;

  public energy: INutrient;
  public fat: INutrient;
  public satFat: INutrient;
  public carbs: INutrient;
  public sugar: INutrient;
  public fiber: INutrient;
  public protein: INutrient;
  public sodium: INutrient;

  public products: IRecipeProduct[];
  public nutritionCalculatedProducts: [] = [];

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly recipeService: RecipeService,
  ) { }

  ngOnInit() {
    this.route.data.subscribe(data => (this.individualRecipe = data.individualRecipe));

    this.energy = this.individualRecipe.nutritionalValue.find(nutrient => nutrient.description === 'Energy');
    this.fat = this.individualRecipe.nutritionalValue.find(nutrient => nutrient.description === 'Total lipid (fat)');
    this.satFat = this.individualRecipe.nutritionalValue.find(nutrient => nutrient.description === 'Fatty acids, total saturated');
    this.carbs = this.individualRecipe.nutritionalValue.find(nutrient => nutrient.description === 'Carbohydrate, by difference');
    this.sugar = this.individualRecipe.nutritionalValue.find(nutrient => nutrient.description === 'Sugars, total');
    this.fiber = this.individualRecipe.nutritionalValue.find(nutrient => nutrient.description === 'Fiber, total dietary');
    this.protein = this.individualRecipe.nutritionalValue.find(nutrient => nutrient.description === 'Protein');
    this.sodium = this.individualRecipe.nutritionalValue.find(nutrient => nutrient.description === 'Sodium, Na');

    this.products = this.individualRecipe.recipeProducts;

    this.products.forEach(product => {
      const selectedMeasure = product.product.measures.find(measure => measure.measure === product.measurment);
      const totalGrams = selectedMeasure.gramsPerMeasure * +product.amount;
      // Provided nutrition data is for 100 grams
      const nutritionMultiplier = totalGrams / 100;
      product.nutritionMultiplier = nutritionMultiplier;
    });
    console.log(this.individualRecipe.id, typeof this.individualRecipe.id);

  }

  editRecipe() {
    this.router.navigate(['/recipes/update-recipe', this.individualRecipe.id]);
  }

  deleteRecipe() {
    if (confirm('Are you sure you want to delete this recipe?')) {
      this.recipeService.deleteRecipe(this.individualRecipe.id).subscribe();
      this.router.navigate(['/recipes']);
    }
  }
}
