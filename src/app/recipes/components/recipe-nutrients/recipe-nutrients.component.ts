import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-recipe-nutrients',
  templateUrl: './recipe-nutrients.component.html',
  styleUrls: ['./recipe-nutrients.component.css']
})
export class RecipeNutrientsComponent {
  private detailed = false;
  @Input() kcal: number;
  @Input() fats: number;
  @Input() carbs: number;
  @Input() sugars: number;
  @Input() allNutrients: [];

  constructor() {}

  showDetailed() {
    if (this.allNutrients) {
      this.detailed = !this.detailed;
      this.allNutrients.splice(21, 1);
    }
  }
}
