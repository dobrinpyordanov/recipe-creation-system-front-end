import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-detailed-nutrients',
  templateUrl: './detailed-nutrients.component.html',
  styleUrls: ['./detailed-nutrients.component.css']
})
export class DetailedNutrientsComponent {
  public show = true;
  @Input() nutrients: [];
  @Output() toggleEvent = new EventEmitter<boolean>();

  constructor() {}

  sendToggle() {
    this.close();
    this.toggleEvent.emit(this.show);
  }
  close() {
    this.show = false;
  }
}
