import { DetailedNutrientsComponent } from './detailed-nutrients.component';

describe('DetailedNutrientsComponent', () => {
  let component: DetailedNutrientsComponent;

  beforeEach(() => {
    component = new DetailedNutrientsComponent();
  });

  it('close() should change the value of close', () => {
    // arrange
    component.show = true;
    // act
    component.close();
    // assert
    expect(component.show).toBe(false);
  });
  it('should emit false', () => {
    // arrange
    spyOn(component.toggleEvent, 'emit');
    // act
    component.sendToggle();
    // assert
    expect(component.toggleEvent.emit).toHaveBeenCalledWith(false);
  });
});
