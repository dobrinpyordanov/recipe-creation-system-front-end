import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ProductDTO } from './interfaces/productDTO';
import { CreateRecipeDTO } from './interfaces/createRecipeDTO';

@Injectable({
  providedIn: 'root'
})
export class RecipeService {
  constructor(private readonly http: HttpClient) { }

  public productsFromFoodGroup(foodGroup: string): Observable<any> {
    return this.http.get(
      `http://localhost:3000/products/?foodGroup=${foodGroup}`
    );
  }
  public searchProduct(product: string): Observable<ProductDTO[]> {
    return this.http.get<ProductDTO[]>(
      `http://localhost:3000/products/?description=${product}&limit=20`
    );
  }
  public createRecipe(recipe: CreateRecipeDTO) {
    return this.http.post('http://localhost:3000/recipes-in-memory', recipe);
  }
  public getAllRecipes(): Observable<CreateRecipeDTO[]> {
    return this.http.get<CreateRecipeDTO[]>(
      'http://localhost:3000/recipes-in-memory'
    );
  }
  public getRecipeById(id): Observable<any> {
    return this.http.get(`http://localhost:3000/recipes-in-memory/${id}`);
  }
  public updateRecipe(id, recipe: CreateRecipeDTO) {
    return this.http.put(
      `http://localhost:3000/recipes-in-memory/${id}`,
      recipe
    );
  }
  public getRecipeByTitle(title): Observable<any> {
    return this.http.get(
      `http://localhost:3000/recipes-in-memory/?title=${title}`
    );
  }

  public deleteRecipe(id) {
    return this.http.delete(
      `http://localhost:3000/recipes-in-memory/${id}`
    );
  }

}
