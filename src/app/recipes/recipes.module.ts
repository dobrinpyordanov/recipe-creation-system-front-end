import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../shared/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RecipeCreateComponent } from './containers/recipe-create/recipe-create.component';
import { RecipesRoutingModule } from './recipes-routing.module';
import { RecipeNutrientsComponent } from './components/recipe-nutrients/recipe-nutrients.component';
import { DetailedNutrientsComponent } from './components/detailed-nutrients/detailed-nutrients.component';
import { AddRecipeComponent } from './components/add-recipe/add-recipe.component';
import { ViewAllRecipesComponent } from './containers/view-all-recipes/view-all-recipes.component';
import { RecipeCardComponent } from './components/recipe-card/recipe-card.component';
import { RecipeComponent } from './containers/recipe/recipe.component';
import { AddProductComponent } from './components';
import { UpdateRecipeComponent } from './containers/update-recipe/update-recipe.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { RecipeService } from './recipe.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from '../interceptors/auth-interceptor';
import { RecipeDetailsComponent } from './components/recipe-details/recipe-details.component';

@NgModule({
  declarations: [
    RecipeCreateComponent,
    AddProductComponent,
    RecipeNutrientsComponent,
    DetailedNutrientsComponent,
    AddRecipeComponent,
    ViewAllRecipesComponent,
    RecipeCardComponent,
    RecipeComponent,
    UpdateRecipeComponent,
    RecipeDetailsComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    RecipesRoutingModule,
    NgxPaginationModule
  ],
  providers: [
    RecipeService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ]
})
export class RecipesModule {}
