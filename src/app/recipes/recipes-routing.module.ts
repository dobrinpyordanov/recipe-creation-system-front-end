import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RecipeCreateComponent } from './containers/';
import { ViewAllRecipesComponent } from './containers';
import { RecipeComponent } from './containers';
import { UpdateRecipeComponent } from './containers';
import { SingleRecipeResolverService } from './resolvers/single-recipe-resolver.service';
import { RecipesResolverService } from './resolvers/recipes-resolver.service';
import { AuthGuard } from '../common/guards/auth.guard';
import { RecipeDetailsComponent } from './components/recipe-details/recipe-details.component';

const routes: Routes = [
  {
    path: '',
    component: RecipeComponent,
    children: [
      { path: '', redirectTo: 'view-all' },
      {
        path: 'create',
        component: RecipeCreateComponent,
        canActivate: [AuthGuard]
      },

      {
        path: 'view-all',
        component: ViewAllRecipesComponent,
        resolve: { recipe: RecipesResolverService },
        canActivate: [AuthGuard]
      },
      {
        path: ':id',
        component: RecipeDetailsComponent,
        resolve: { individualRecipe: SingleRecipeResolverService },
        canActivate: [AuthGuard]
      },
      {
        path: 'update-recipe/:id',
        component: UpdateRecipeComponent,
        resolve: { recipe: SingleRecipeResolverService },
        canActivate: [AuthGuard]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RecipesRoutingModule {}
