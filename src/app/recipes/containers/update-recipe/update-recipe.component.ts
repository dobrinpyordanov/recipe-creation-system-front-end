import { NotificatorService } from 'src/app/core/services/notificator.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ICreateRecipeProduct } from 'src/app/common/interfaces/createRecipeProduct.interface';
import { ISelectedNutrient } from 'src/app/common/interfaces/selectedNutrient.interface';
import { ActivatedRoute, Router } from '@angular/router';
import { CreateRecipeDTO } from '../../interfaces/createRecipeDTO';
import { RecipeService } from '../../recipe.service';

@Component({
  selector: 'app-update-recipe',
  templateUrl: './update-recipe.component.html',
  styleUrls: ['./update-recipe.component.css']
})
export class UpdateRecipeComponent implements OnInit {
  public recipeToUpdate: CreateRecipeDTO;
  public productsToUpdate: ICreateRecipeProduct[] & any = [];
  private recipeNameControl = new FormControl('', [Validators.required]);
  private showDetailed = false;
  private newIngredient = [''];
  private newRecipe = [];
  private recipeIngreds: ICreateRecipeProduct[] = [];
  private allNutrients = [];
  private wholeRecipeNutrients: ISelectedNutrient[];
  private prod;
  private nutrients: ISelectedNutrient[];
  private show = false;
  private title: string;
  private recipeId: string;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly recipeService: RecipeService,
    private readonly notificator: NotificatorService,
    private readonly router: Router,
  ) { }

  ngOnInit() {
    this.route.data.subscribe(data => (this.recipeToUpdate = data.recipe));
    this.productsToUpdate = this.recipeToUpdate.recipeProducts;
    this.title = this.recipeToUpdate.title;
    this.wholeRecipeNutrients = this.recipeToUpdate.nutritionalValue;
    this.recipeId = this.recipeToUpdate.id;
    this.recipeIngreds = this.recipeToUpdate.recipeProducts;
    this.newRecipe = this.recipeToUpdate.childRecipes;
  }

  receiveToggle($event) {
    this.show = $event;
  }
  receiveShow($event) {
    this.show = $event;
  }
  receiveNutrients($event, index) {
    this.nutrients = $event;
    this.allNutrients.splice(index, 1, this.nutrients);
    this.showDetailed = true;
    console.log(this.allNutrients, 'all nutrs');
    console.log(this.nutrients);

    this.calculateWholeRecipeNutrients();
  }
  calculateWholeRecipeNutrients() {
    const testArray = [
      [],
      [],
      [],
      [],
      [],
      [],
      [],
      [],
      [],
      [],
      [],
      [],
      [],
      [],
      [],
      [],
      [],
      [],
      [],
      [],
      [],
      []
    ];
    for (let i = 0; i < 22; i++) {
      this.allNutrients.forEach(element => {
        testArray[i].push(Number(element[i].value));
      });
    }
    const wholeRecipeNutrition = [];
    testArray.forEach(element => {
      wholeRecipeNutrition.push(element.reduce((a, b) => a + b, 0));
    });
    console.log(wholeRecipeNutrition, 'array');
    const whatTheFck = [
      { description: 'Protein', value: '0.42', unit: 'g' },
      { description: 'Total lipid (fat)', value: '0.15', unit: 'g' },
      { description: 'Carbohydrate, by difference', value: '0.88', unit: 'g' },
      { description: 'Energy', value: '4.14', unit: 'kcal' },
      { description: 'Sugars, total', value: '0.07', unit: 'g' },
      { description: 'Fiber, total dietary', value: '0.58', unit: 'g' },
      { description: 'Calcium, Ca', value: '7.20', unit: 'mg' },
      { description: 'Iron, Fe', value: '0.30', unit: 'mg' },
      { description: 'Phosphorus, P', value: '1.80', unit: 'mg' },
      { description: 'Potassium, K', value: '7.20', unit: 'mg' },
      { description: 'Sodium, Na', value: '422.64', unit: 'mg' },
      { description: 'Vitamin A, IU', value: '24.84', unit: 'IU' },
      {
        description: 'Vitamin E (alpha-tocopherol)',
        value: '0.16',
        unit: 'mg'
      },
      { description: 'Vitamin D', value: '0.00', unit: 'IU' },
      {
        description: 'Vitamin C, total ascorbic acid',
        value: '0.77',
        unit: 'mg'
      },
      { description: 'Vitamin B-12', value: '0.00', unit: 'µg' },
      { description: 'Folic acid', value: '0.00', unit: 'µg' },
      { description: 'Cholesterol', value: '0.00', unit: 'mg' },
      { description: 'Fatty acids, total trans', value: '0.00', unit: 'g' },
      { description: 'Fatty acids, total saturated', value: '0.04', unit: 'g' },
      {
        description: 'Fatty acids, total monounsaturated',
        value: '0.01',
        unit: 'g'
      },
      {
        description: 'Fatty acids, total polyunsaturated',
        value: '0.05',
        unit: 'g'
      }
    ];
    // const wtf = this.allNutrients[0];
    for (let i = 0; i < 22; i++) {
      whatTheFck[i].value = testArray[i].reduce((a, b) => a + b, 0);
    }

    this.wholeRecipeNutrients = whatTheFck;
  }
  receiveProduct($event, index) {
    this.prod = $event;
    console.log(this.prod);
    console.log(index);

    this.recipeIngreds.splice(index, 1, this.prod);
  }

  test() {
    this.productsToUpdate.push('');
  }
  test2() {
    this.newRecipe.push('');
    console.log(this.prod);

    console.log(this.recipeIngreds);
  }
  updateRecipe() {
    const recipeToCreate: CreateRecipeDTO = {
      id: 'usslessid',
      title: this.title,
      recipeProducts: this.recipeIngreds,
      nutritionalValue: this.wholeRecipeNutrients,
      childRecipes: this.newRecipe,
      recipeCategory: 'main'
    };
    this.recipeService.updateRecipe(this.recipeId, recipeToCreate).subscribe();
    console.log(recipeToCreate);
    this.notificator.success('Recipe updated!');
    this.router.navigate(['/recipes', this.recipeId]);
  }

  returnToSingleRecipeView() {
    this.router.navigate(['/recipes', this.recipeId]);
  }
}
