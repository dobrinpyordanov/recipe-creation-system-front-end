// import { IViewIndividualRecipe } from './../../../common/interfaces/view-individual-recipe.interface';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-view-all-recipes',
  templateUrl: './view-all-recipes.component.html',
  styleUrls: ['./view-all-recipes.component.css']
})
export class ViewAllRecipesComponent implements OnInit {
  public allRecipes;
  // private individualRecipe: IViewIndividualRecipe;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router
  ) {}

  ngOnInit() {
    this.route.data.subscribe(data => (this.allRecipes = data.recipe));
  }

  // findIndividualRecipeById(recipeId: string) {
  //   this.individualRecipe = this.allRecipes.find(recipe => recipe.id === recipeId);
  // }

  redirectToRecipeDetails(recipeId: string) {
    this.router.navigate(['/recipes', recipeId]);
  }
}
