import { Component, OnInit, ViewChild } from '@angular/core';
import { ICreateRecipeProduct } from 'src/app/common/interfaces/createRecipeProduct.interface';
import { ISelectedNutrient } from 'src/app/common/interfaces/selectedNutrient.interface';
import { FormControl, Validators } from '@angular/forms';
import { CreateRecipeDTO } from '../../interfaces/createRecipeDTO';
import { RecipeService } from '../../recipe.service';
import { flatMap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { NotificatorService } from 'src/app/core/services/notificator.service';

@Component({
  selector: 'app-recipe-create',
  templateUrl: './recipe-create.component.html',
  styleUrls: ['./recipe-create.component.css']
})
export class RecipeCreateComponent {
  // ingredientsArr: CreateRecipeProduct[] = [];
  private title = 'no title';
  private category = 'no category';
  private recipeNameControl = new FormControl('', [Validators.required]);
  private CategoryNameControl = new FormControl('', [Validators.required]);
  private showDetailed = false;
  private newIngredient = [''];
  private newRecipe = [];
  private recipeIngreds: ICreateRecipeProduct[] = [];
  private allKcal = [];
  private allFats = [];
  private allCarbs = [];
  private allSugars = [];
  private allNutrients = [];
  private wholeRecipeNutrients: ISelectedNutrient[] = [];
  private prod;
  private kcalParent: number;
  private fatParent: number;
  private carbParent: number;
  private sugarParent: number;
  private nutrients: ISelectedNutrient[];
  private show = false;
  private recipeIsReady = false;
  private recipes = [];

  constructor(
    private readonly recipeService: RecipeService,
    private readonly router: Router,
    private readonly notificator: NotificatorService
  ) {}

  setTitle(title) {
    this.title = title;
  }
  setCategory(input) {
    this.category = input;
  }

  updateCalories() {
    this.kcalParent = this.allKcal.reduce((a, b) => a + b, 0);
  }
  updateFats() {
    this.fatParent = this.allFats.reduce((a, b) => a + b, 0);
  }
  updateCarbs() {
    this.carbParent = this.allCarbs.reduce((a, b) => a + b, 0);
  }
  updateSugars() {
    this.sugarParent = this.allSugars.reduce((a, b) => a + b, 0);
  }
  closeRecipe($event, index) {
    const test = [0, 1, 2, 3, 4];
    test.splice(index, 1);
    this.newRecipe.splice(index, 1);
  }
  receiveToggle($event) {
    this.show = $event;
  }
  receiveShow($event) {
    this.show = $event;
  }
  receiveRecipe($event) {
    this.recipes.push($event);
  }
  receiveNutrients($event, index) {
    this.nutrients = $event;
    this.allNutrients.splice(index, 1, this.nutrients);
    this.showDetailed = true;
    this.calculateWholeRecipeNutrients();
  }
  receiveNutrientsFromrecipe($event, i) {
    this.nutrients = $event;
    const lenght = this.allNutrients.length;
    this.allNutrients.push(this.nutrients);
    this.calculateWholeRecipeNutrients();
  }
  calculateWholeRecipeNutrients() {
    const testArray = [
      [],
      [],
      [],
      [],
      [],
      [],
      [],
      [],
      [],
      [],
      [],
      [],
      [],
      [],
      [],
      [],
      [],
      [],
      [],
      [],
      [],
      []
    ];
    for (let i = 0; i < 22; i++) {
      this.allNutrients.forEach(element => {
        testArray[i].push(Number(element[i].value));
      });
    }
    const wholeRecipeNutrition = [];
    testArray.forEach(element => {
      wholeRecipeNutrition.push(element.reduce((a, b) => a + b, 0));
    });
    const whatTheFck = [
      { description: 'Protein', value: '0.42', unit: 'g' },
      { description: 'Total lipid (fat)', value: '0.15', unit: 'g' },
      { description: 'Carbohydrate, by difference', value: '0.88', unit: 'g' },
      { description: 'Energy', value: '4.14', unit: 'kcal' },
      { description: 'Sugars, total', value: '0.07', unit: 'g' },
      { description: 'Fiber, total dietary', value: '0.58', unit: 'g' },
      { description: 'Calcium, Ca', value: '7.20', unit: 'mg' },
      { description: 'Iron, Fe', value: '0.30', unit: 'mg' },
      { description: 'Phosphorus, P', value: '1.80', unit: 'mg' },
      { description: 'Potassium, K', value: '7.20', unit: 'mg' },
      { description: 'Sodium, Na', value: '422.64', unit: 'mg' },
      { description: 'Vitamin A, IU', value: '24.84', unit: 'IU' },
      {
        description: 'Vitamin E (alpha-tocopherol)',
        value: '0.16',
        unit: 'mg'
      },
      { description: 'Vitamin D', value: '0.00', unit: 'IU' },
      {
        description: 'Vitamin C, total ascorbic acid',
        value: '0.77',
        unit: 'mg'
      },
      { description: 'Vitamin B-12', value: '0.00', unit: 'µg' },
      { description: 'Folic acid', value: '0.00', unit: 'µg' },
      { description: 'Cholesterol', value: '0.00', unit: 'mg' },
      { description: 'Fatty acids, total trans', value: '0.00', unit: 'g' },
      { description: 'Fatty acids, total saturated', value: '0.04', unit: 'g' },
      {
        description: 'Fatty acids, total monounsaturated',
        value: '0.01',
        unit: 'g'
      },
      {
        description: 'Fatty acids, total polyunsaturated',
        value: '0.05',
        unit: 'g'
      }
    ];
    // const wtf = this.allNutrients[0];
    for (let i = 0; i < 22; i++) {
      whatTheFck[i].value = testArray[i].reduce((a, b) => a + b, 0);
    }
    this.wholeRecipeNutrients = whatTheFck;
  }
  receiveCalories($event, index) {
    this.kcalParent = $event;
    this.allKcal.splice(index, 1, this.kcalParent);
    this.updateCalories();
  }
  receiveCaloriesFromRecipe($event) {
    const calories = $event;
    this.allKcal.push(calories);
    this.updateCalories();
  }
  receiveFats($event, index) {
    this.fatParent = $event;
    this.allFats.splice(index, 1, this.fatParent);
    this.updateFats();
  }
  receiveCarbs($event, index) {
    this.carbParent = $event;
    this.allCarbs.splice(index, 1, this.carbParent);
    this.updateCarbs();
  }
  receiveSugars($event, index) {
    this.sugarParent = $event;
    this.allSugars.splice(index, 1, this.sugarParent);
    this.updateSugars();
  }
  receiveProduct($event, index) {
    this.prod = $event;
    this.recipeIsReady = true;

    this.recipeIngreds.splice(index, 1, this.prod);
  }

  test() {
    this.newIngredient.push('');
    // this.ingredientsArr.push({
    //   description: '',
    //   measurment: '',
    //   quantity: 1
    // });
  }
  test2() {
    this.newRecipe.push('');
    // console.log(this.prod, 'prod');

    // console.log(this.recipeIngreds, 'recipeIngreds');

    // this.bKcal.next(this.kcalParent);
    // console.log(this.bKcal.value);
  }
  createRecipe() {
    const recipeToCreate: CreateRecipeDTO = {
      id: 'usslessid',
      title: this.title,
      recipeProducts: this.recipeIngreds,
      nutritionalValue: this.wholeRecipeNutrients,
      childRecipes: this.recipes,
      recipeCategory: this.category
    };
    this.recipeService.createRecipe(recipeToCreate).subscribe(
      () => {
        this.notificator.success(`Recipe '${this.title}' was created`);
        this.router.navigate(['recipes/view-all']);
      },
      () => this.notificator.error('Something went wrong')
    );
    console.log(recipeToCreate);
  }
}
