// import { async } from 'q';
// import { TestBed, ComponentFixture } from '@angular/core/testing';
// import {
//   RecipeCreateComponent,
//   ViewAllRecipesComponent,
//   RecipeComponent,
//   UpdateRecipeComponent
// } from '../../containers';
// import { CommonModule } from '@angular/common';
// import { MaterialModule } from 'src/app/shared/material.module';
// import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { RecipesRoutingModule } from '../../recipes-routing.module';
// import { NgxPaginationModule } from 'ngx-pagination';
// import {
//   NO_ERRORS_SCHEMA,
//   CUSTOM_ELEMENTS_SCHEMA,
//   Component
// } from '@angular/core';
// import { HttpClientModule } from '@angular/common/http';
// import { Router } from '@angular/router';
// import { NotificatorService } from 'src/app/core/services/notificator.service';
// import { RecipeService } from '../../recipe.service';
// import { RecipeDetailsComponent } from '../../components';

// describe('RecipeCreateComponent', () => {
//   let fixture: ComponentFixture<RecipeCreateComponent>;
//   let mockRecipeService = jasmine.createSpyObj('RecipeService', [
//     'getRecipeByTitle'
//   ]);
//   let mockRouter = jasmine.createSpyObj('Router', ['navigate']);
//   let mockNotificator = jasmine.createSpyObj('Notificator', [
//     'success',
//     'error'
//   ]);
//   @Component({
//     selector: 'app-recipe',
//     template: '<div></div>'
//   })
//   class FakeRecipeComponent {
//     constructor() {}
//   }

//   beforeEach(async(() => {
//     TestBed.configureTestingModule({
//       declarations: [RecipeCreateComponent, RecipeComponent],
//       imports: [
//         CommonModule,
//         MaterialModule,
//         FormsModule,
//         ReactiveFormsModule,
//         RecipesRoutingModule,
//         NgxPaginationModule,
//         HttpClientModule
//       ],
//       providers: [
//         {
//           provide: Router,
//           useValue: mockRouter
//         },
//         {
//           provide: NotificatorService,
//           useValue: mockNotificator
//         },
//         {
//           provide: RecipeService,
//           useValue: mockRecipeService
//         }
//       ]
//       // schemas: [NO_ERRORS_SCHEMA]
//     });
//     fixture = TestBed.createComponent(RecipeCreateComponent);
//   }));

//   it('sadasda', () => {
//     expect(true).toBe(true);
//   });
// });
