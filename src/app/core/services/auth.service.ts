import { UserLogin } from './../../common/interfaces/user-login.interface';
import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private isLoggedInSubject$ = new BehaviorSubject<boolean>(
    this.isUserAuthenticated()
  );

  constructor(
    private readonly http: HttpClient,
    private readonly storage: StorageService
  ) { }

  public get isLoggedIn$(): Observable<boolean> {
    return this.isLoggedInSubject$.asObservable();
  }

  public login(user: UserLogin): Observable<any> {
    return this.http.post('http://localhost:3000/session', user).pipe(
      tap(data => {
        this.storage.setItem('token', data.token);
        this.isLoggedInSubject$.next(true);
      })
    );
  }

  public logout(): Observable<any> {
    return this.http.delete('http://localhost:3000/session').pipe(
      tap(() => {
        this.storage.removeItem('token');
        this.isLoggedInSubject$.next(false);
      })
    );
  }

  private isUserAuthenticated(): boolean {
    return !!this.storage.getItem('token');
  }
}
