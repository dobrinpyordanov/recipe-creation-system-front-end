import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NotificatorService } from './services/notificator.service';
import { AuthService } from './services/auth.service';
import { StorageService } from './services/storage.service';

@NgModule({
  imports: [HttpClientModule, CommonModule],
  providers: [NotificatorService, AuthService, StorageService]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parent: CoreModule) {
    if (parent) {
      throw new Error('Core module is already provided!');
    }
  }
}
