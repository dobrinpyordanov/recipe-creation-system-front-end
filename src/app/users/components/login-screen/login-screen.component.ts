import { NotificatorService } from './../../../core/services/notificator.service';
import { AuthService } from './../../../core/services/auth.service';
import { UserLogin } from './../../../common/interfaces/user-login.interface';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-screen',
  templateUrl: './login-screen.component.html',
  styleUrls: ['./login-screen.component.css']
})
export class LoginScreenComponent implements OnInit {

  private loginForm: FormGroup;

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly authService: AuthService,
    private readonly notificator: NotificatorService,
    private readonly router: Router
  ) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
    });
  }

  public login(username: string, password: string) {
    const user: UserLogin = { username, password };

    this.authService.login(user).subscribe(
      () => {
        this.notificator.success(`Successful login!`);
        this.router.navigate(['/recipes']);
      },
      () => {
        this.notificator.error('Login failed!');
      }
    );
  }
}
