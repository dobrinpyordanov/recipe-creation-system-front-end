import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from './core/services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  public isLoggedInSubscription: Subscription;
  public isLoggedIn: boolean;

  public constructor(
    private readonly authService: AuthService,
  ) { }

  public ngOnInit(): void {
    this.isLoggedInSubscription = this.authService.isLoggedIn$.subscribe(
      isLogged => {
        this.isLoggedIn = isLogged;
      }
    );
  }

  public ngOnDestroy(): void {
    this.isLoggedInSubscription.unsubscribe();
  }
}
