export interface ISelectedNutrient {
  description: string;
  value: string;
  unit: string;
}
