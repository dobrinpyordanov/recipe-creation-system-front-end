import { IProduct } from './product.interface';

export interface IRecipeProduct {
  amount: string;
  measurment: string;
  product: IProduct;
  nutritionMultiplier: number;
}
