import { INutrient } from './nutrient.interface';
import { IRecipeProduct } from './recipe-product.interface';

export interface IChildRecipe {
  id: string;
  nutritionalValue: INutrient[];
  recipeCategory: string;
  recipeProducts: IRecipeProduct[];
  title: string;
}
