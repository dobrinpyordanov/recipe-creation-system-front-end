import { INutrition } from './nutrition.interface';
import { IMeasure } from './measurement.interface';

export interface IProduct {
  description: string;
  foodGroup: string;
  measures: IMeasure[];
  nutrition: INutrition;
}
