import { IProduct } from './product.interface';

export interface ICreateRecipeProduct {
  product: IProduct;
  measurment: string;
  amount: number;
  gramsPerSelectedMeasure: number;
}
