import { INutrient } from './nutrient.interface';
import { IRecipeProduct } from './recipe-product.interface';
import { IChildRecipe } from './child-recipe.interface';

export interface IViewIndividualRecipe {
  id: string;
  nutritionalValue: INutrient[];
  recipeCategory: string;
  recipeProducts: IRecipeProduct[];
  title: string;
  childRecipes: IChildRecipe[];
}
