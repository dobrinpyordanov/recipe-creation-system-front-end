export enum RecipeCategory {
  Soup = 'Soup',
  Appetizer = 'Appetizer',
  Salad = 'Salad',
  Bread = 'Bread',
  Drink = 'Drink',
  Dessert = 'Dessert',
  MainDish = 'Main Dish',
  Sauce = 'Sauce',
  SideDish = 'Side Dish',
}
