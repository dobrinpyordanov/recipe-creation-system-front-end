import { StorageService } from './../../core/services/storage.service';
import { AuthService } from './../../core/services/auth.service';
import { Injectable } from '@angular/core';
import {
  Router,
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import { NotificatorService } from 'src/app/core/services/notificator.service';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
  constructor(
    private router: Router,
    private storageService: StorageService,
    private readonly notificator: NotificatorService
  ) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const currentUser = this.storageService.getItem('token');
    if (currentUser) {
      // authorised so return true
      return true;
    }

    // not logged in so redirect to login page with the return url
    this.notificator.error(`You're unauthorized to access this page!`);
    this.router.navigate(['/users/login']);
    return false;
  }
}
