import { NotificatorService } from 'src/app/core/services/notificator.service';
import { AuthService } from './../../core/services/auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {
  private toggle = false;

  constructor(
    private readonly authService: AuthService,
    private readonly notificator: NotificatorService,
    private readonly router: Router,
  ) { }

  ngOnInit() {
  }

  toggleButton() {
    this.toggle = !this.toggle;
  }

  public logout(): void {
    this.authService
      .logout()
      .subscribe(
        () => {
          this.notificator.success('Successful logout!');
          this.router.navigate(['/users/login']);
        },
        () => this.notificator.error('Logout failed!')
      );
  }
}
