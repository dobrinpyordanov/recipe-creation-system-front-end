import { NavBarComponent } from './nav-bar.component';
import { of, Observable } from 'rxjs';
import { throwError } from 'rxjs';

describe('NavBarComponent', () => {
  let component: NavBarComponent;
  let mockAuthService;
  let mockNotificatorService;
  let mockRouter;

  beforeEach(() => {
    mockAuthService = jasmine.createSpyObj('AuthService', ['logout']);
    mockNotificatorService = jasmine.createSpyObj('NOtificator', [
      'success',
      'error'
    ]);
    mockRouter = jasmine.createSpyObj('Router', ['navigate']);
    component = new NavBarComponent(
      mockAuthService,
      mockNotificatorService,
      mockRouter
    );
  });

  it('logout should be called once when logout()', () => {
    // arrange
    mockAuthService.logout.and.returnValue(of(true));
    // act
    component.logout();
    // assert
    expect(mockAuthService.logout).toHaveBeenCalledTimes(1);
  });

  it('notificator should be called with right message on success', () => {
    // arrange
    mockAuthService.logout.and.returnValue(of(true));
    // act
    component.logout();
    // assert
    expect(mockNotificatorService.success).toHaveBeenCalledWith(
      'Successful logout!'
    );
  });

  it('notificator should be called with right message on error', () => {
    // arrange
    mockAuthService.logout.and.returnValue(throwError({ status: 418 }));
    // act
    component.logout();
    // assert
    expect(mockNotificatorService.error).toHaveBeenCalledWith('Logout failed!');
  });

  it('router should navigate to right path', () => {
    // arrange
    mockAuthService.logout.and.returnValue(of(true));
    // act
    component.logout();
    // assert
    expect(mockRouter.navigate).toHaveBeenCalledWith(['/users/login']);
  });
});
